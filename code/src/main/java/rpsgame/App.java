// Jaypee Tello || 2232311
package rpsgame;
import java.util.Scanner;
import backend.RpsGame;

/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //TODO
        Scanner scan = new Scanner(System.in);
        boolean quit = false;
        RpsGame rpsGame = new RpsGame();

        while (!quit) {
            System.out.println("rock, paper or scissors? or quit?");
            String playerChoice = scan.nextLine();

            if (playerChoice.equals("quit")) {
                quit = true;
                System.out.println("Wins: " + rpsGame.getWins());
                System.out.println("Ties: " + rpsGame.getTies());
                System.out.println("Losses: " + rpsGame.getLosses());
            }
            else {
                System.out.println(rpsGame.playRound(playerChoice));
            }
        }
        scan.close();
    }
}
