// Jaypee Tello || 2232311
package backend;
import java.util.Random;

public class RpsGame {
    //TODO
    private int wins;
    private int ties;
    private int losses;
    private Random ran;

    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
        this.ran = new Random();
    }

    public int getWins() {
        return this.wins;
    }

    public int getTies() {
        return this.ties;
    }
    
    public int getLosses() {
        return this.losses;
    }

    public String playRound(String playerChoice) {
        int randomNum = ran.nextInt(3);
        String computerChoice = "";
        String winner = "";

        if (randomNum == 0) {
            computerChoice = "rock";
        }
        else if (randomNum == 1) {
            computerChoice = "scissors";
        }
        else if (randomNum == 2) {
            computerChoice = "paper";
        }

        if (playerChoice.equals(computerChoice)) {
            this.ties++;
            winner = "It's a tie! Both played " + playerChoice;
        }
        else if ((playerChoice.equals("rock") && computerChoice.equals("scissors")) 
        || (playerChoice.equals("paper") && computerChoice.equals("rock")) 
        || (playerChoice.equals("scissors") && computerChoice.equals("paper"))) {
            this.wins++;
            winner = "Player plays " + playerChoice + " and the player won!";
        }
        else if ((computerChoice.equals("rock") && playerChoice.equals("scissors"))
        || (computerChoice.equals("paper") && playerChoice.equals("rock"))
        || (computerChoice.equals("scissors") && playerChoice.equals("paper"))) {
            this.losses++;
            winner = "Computer plays " + computerChoice + " and the computer won!";
        }
        return winner;
    }
}
